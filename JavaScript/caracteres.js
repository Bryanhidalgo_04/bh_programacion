//Notacion de corchetes: Enesimo caracter
var miCadena = "JavaScript";
//Cadena =   J a v a S c r i p t
//Indices =  0 1 2 3 4 5 6 7 8 9
console.log(miCadena[0]);
console.log(miCadena[1]);
console.log(miCadena[2]);
console.log(miCadena[3]);
console.log(miCadena[4]);
console.log(miCadena[5]);
console.log(miCadena[6]);
console.log(miCadena[7]);
console.log(miCadena[8]);
console.log(miCadena[9]);
console.log(miCadena[10]);//Al no haber un dato o una letra en la casilla supuesta 10 o -3 o 1.2, este no mostrara el error, pero mostrara como retirno undifined

//Notacion de corchetes: Ultimo caracter

var miCadena2;
/*
 El ultimo indice siempre es longitud de la cadena -1 debido a que se empieza a contar desde 0
 miCadena.lenght para "Programacion" es 12
 el ultimo indice es 11
 Cadena:   J a v a S c r i p t
 Indices:  0 1 2 3 4 5 6 7 8 9
 */
miCadena2 = "JavaScript"
//Longitud de la cadena -1
console.log(miCadena2[miCadena2.length -1]);// cadena.lenght -1 se usa para obtener el ultimo caracter(El contador de la cadena empieza desde 1 por eso va el -1)

//Acceder a la cadena para obtener un caracter especifico
/*El penultimo indice es la longitud -2 y asi para obtener el caracter deseado*/
let n = 2;
console.log(miCadena2[miCadena2.length - n]);//De derecha a izquierda 


