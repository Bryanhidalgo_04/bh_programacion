//CONCATENAR CADENA DE CARACTERES
//PALABRAS EN BLANCO
let miSustantivo = "bicicleta";
let miAdjetivo = "pequeña";
let miVerbo = "volo";
let miAdverbio = "lentamente";
/* Concatenar las cadenas para crear una cadena que muestre un mensaje. Puedes cambiar los valores de las variables
Ejemplo: El perro negro corrio rapidamente a la tienda.
La bivivleta pequeña volo a la tienda lentamente. */

var palabrasEnBlanco = "La " +miSustantivo+ " " + miAdjetivo +" "+ miVerbo+" "+ miAdverbio + " a la tienda.";
console.log(palabrasEnBlanco);
