//ARREGLOS
//Estructura de datos que permite almacenar multiples valores en una misma estructura
console.log("\t Arreglos");
let miArreglo = ["Bryan",23,"Christopher",25,"Giorgio",24];
console.log(miArreglo);

let estudiantes = ["Bryan","Christopher","Madeline","Giorgio"];
console.log(estudiantes);

let calificaciones = [10, 9, 8, 8, 10, 15];
console.log(calificaciones);

//ARREGLOS ANIDADOS
console.log("\t Arreglos anidados y recorrida de arreglos anidados");

var listaEstudiante = [["Bryan", 23],["Christopher", 25],["Giorgio", 24]];
console.log(listaEstudiante);/*En freeCodeCamp muestra cada datos del arreglo dentro del otro arreglo, pero en realidad se debe 
recorrer el array dentro de dos for */
for(x=0; x<listaEstudiante.length; x++){
    let texto = "";
    for(y=0; y<listaEstudiante[x].length; y++){
        texto = texto + listaEstudiante[x] [y] + " ";
    }
    console.log(texto);
}
var listaProductos = [["Zapatos", 99,"S134"],["Camisa", 25, "S399","En stock"],["Gorra", 5, "S845"]];
for(x=0; x<listaProductos.length; x++){
    let texto = "";
    for(y=0; y<listaProductos[x].length; y++){
        
        texto = texto + listaProductos[x] [y] +"\t ";
        
    }
    console.log(texto); 
}

//ACCEDER A LOS ELEMENTOS DE UN ARREGLO
console.log("\t Acceso a arreglos Parte 1");
let miArreglo2 = [10,20,30];
/*
Arreglo: [10,20,30]
Indices:   0  1  2
*/
console.log(miArreglo2[0]);//10
console.log(miArreglo2[1]);//20
console.log(miArreglo2[2]);//30
let suma = miArreglo2[1] + miArreglo2[2];
console.log(suma);

//ACCEDER A ARREGLOS MULTIDIMENSIONALES
console.log("\t Acceso a arreglos Parte 2");
let miArreglo3 =[[1,2,3], [4,5,6], [7,8,9]];
/*Arreglos =            [1,2,3], [4,5,6], [7,8,9] 
  Indices: =               0        1        2
  Indices internos       0 1 2    0 1 2    0 1 2 
  */
console.log(miArreglo3[0][1]);
console.log(miArreglo3[1][2]);
console.log(miArreglo3[2][2]);


//MODIFICAR LOS ELEMENTOS DE UN ARREGLO
console.log("\t Modificar los elementos de un arreglo");
//Arreglo: [10,20,30]
miArreglo2[1] = 200;
miArreglo2[2] = "Holaa";
//miArreglo2[2] = [1,2,3]; Tambien se puede hacer esto pero se recorre de una forma diferente
console.log(miArreglo2[1]);
console.log(miArreglo2[2]);

//CUATRO METODOS PRINCIPALES DE UN ARREGLO NORMAL
//.Push = Permite añadir un elemento al final del arreglo
console.log("\t Arreglo: metodo .push ");
let estaciones = ["Invierno", "Otoño", "Primavera"];
estaciones.push("Verano");//Añadir un elemento al final del arreglo
console.log(estaciones);

 //.pop() = remueve el ultimo elemento de un arreglo, Ademas de remover el elemento del arreglo lo puede retornar a una variable y guardarlo
 console.log("\t Arreglo: metodo .pop() ");
 let estacion = estaciones.pop();
 console.log(estaciones);
 console.log(estacion);

 //.shift() = remueve el primer elemento de un arreglo, Ademas de remover el elemento del arreglo lo puede retornar a una variable y guardarlo
 console.log("\t Arreglo: metodo .shift() ");
 let fuera = estaciones.shift();
 console.log(estaciones);
 console.log(fuera);

 //.unshift() Agrega un elemento al principio de un arreglo
 estaciones.unshift("Invierno");//Agrega al principio de un arreglo
 console.log(estaciones);


