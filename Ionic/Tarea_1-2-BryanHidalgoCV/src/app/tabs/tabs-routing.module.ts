import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'informacion',
        loadChildren: () => import('../informacion/informacion.module').then(m => m.InformacionPageModule)
      },
      {
        path: 'titulos',
        loadChildren: () => import('../titulos/titulos.module').then(m => m.TitulosPageModule)
      },
      {
        path: 'tab1',
        loadChildren: () => import('../tab1/tab1.module').then(m => m.Tab1PageModule)
      },
      {
        path: 'contactos',
        loadChildren: () => import('../contactos/contactos.module').then(m => m.ContactosPageModule)
      },
      {
        path: '',
        redirectTo: '/tabs/informacion',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/tab1',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class TabsPageRoutingModule {}
