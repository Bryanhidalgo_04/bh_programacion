import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';


@Component({
  selector: 'app-userlist',
  templateUrl: './userlist.page.html',
  styleUrls: ['./userlist.page.scss'],
})
export class UserlistPage implements OnInit {
  characters =[]

  constructor(
    private http: HttpClient
  ) { }

  ngOnInit() {
    this.http.get<any>('https://restcountries.com/v3.1/all')//devuelvo un dato de tipo any para que la respuesta se tome como cualquier cosa para poder guardarlo dentro del arreglo
    .subscribe(res =>{
      console.log(res);
      this.characters = res.results;
    })
  }
}
