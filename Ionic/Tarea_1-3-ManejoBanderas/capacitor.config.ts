import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'Tarea_1-3-ManejoBanderas',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
