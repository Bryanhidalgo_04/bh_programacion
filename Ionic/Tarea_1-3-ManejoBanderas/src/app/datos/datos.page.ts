import { Component} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { CountryPage } from '../country/country.page';
import { proveedor } from '../proveedor/remoteservise';

@Component({
  selector: 'app-datos',
  templateUrl: './datos.page.html',
  styleUrls: ['./datos.page.scss'],
})
export class DatosPage {
  paises
  tipo:string ="all";
  dato:string =" ";
  constructor(    
    private actiatedRoute: ActivatedRoute,
    private http: HttpClient, 
    public proveedorBandera:proveedor
    ) { this.getList(); }

    getList(){
      this.proveedorBandera.obtenerDatos(this.tipo,this.dato)
      .subscribe(
        (data)=>{this.paises = data;},
        (error)=>{console.log(error);}
      )
    }

}
