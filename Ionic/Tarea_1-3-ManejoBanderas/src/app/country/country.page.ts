import { Component} from '@angular/core';
import { NavController } from '@ionic/angular';
import { proveedor } from '../proveedor/remoteservise';

@Component({
  selector: 'app-country',
  templateUrl: './country.page.html',
  styleUrls: ['./country.page.scss'],
})
export class CountryPage  {
  paises;
  tipo:string = "all";
  dato:string =" ";
  constructor(public navCtrl: NavController, public provedorBandera:proveedor) { this.getList();}
  getList(){
    this.provedorBandera.obtenerDatos(this.tipo,this.dato)
    .subscribe(
      (data)=>{this.paises = data;},
      (error)=>{console.log(error);}
    )
  }



}
