import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DatoPaisPage } from './dato-pais.page';

const routes: Routes = [
  {
    path: '',
    component: DatoPaisPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DatoPaisPageRoutingModule {}
