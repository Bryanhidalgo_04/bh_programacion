import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { proveedor } from '../proveedor/remoteservise';
@Component({
  selector: 'app-dato-pais',
  templateUrl: './dato-pais.page.html',
  styleUrls: ['./dato-pais.page.scss'],
})
export class DatoPaisPage {
  tipo: string = "capital";
  dato: string = " ";
  paises;
  constructor(public provedorBandera:proveedor, private activateRoute :ActivatedRoute) { this.getList()}

  getList(){
    this.dato = this.activateRoute.snapshot.paramMap.get('id')
    if(this.dato === "undefined"){
      this.provedorBandera.obtenerDatos("name","Bouvet Island")
    .subscribe(
      (data)=>{this.paises = data;},
      (error)=>{console.log(error);}
    )
    }else{
      this.provedorBandera.obtenerDatos(this.tipo,this.dato)
      .subscribe(
        (data)=>{this.paises = data;},
        (error)=>{console.log(error);}
      )
    }
    
  }

}
