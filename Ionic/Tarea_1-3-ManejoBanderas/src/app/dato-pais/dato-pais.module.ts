import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DatoPaisPageRoutingModule } from './dato-pais-routing.module';

import { DatoPaisPage } from './dato-pais.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DatoPaisPageRoutingModule
  ],
  declarations: [DatoPaisPage]
})
export class DatoPaisPageModule {}
