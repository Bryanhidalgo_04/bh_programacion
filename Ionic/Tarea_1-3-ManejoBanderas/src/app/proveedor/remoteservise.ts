import {HttpClient} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

@Injectable({providedIn:'root'})

export class proveedor{
    ruta = 'https://restcountries.com/v3.1';
    constructor(public http: HttpClient){
        console.log("Hello remoteserviceproveedor proveedor")
    }
    obtenerDatos(tipo: string ,dato: string){
        return this.http.get(this.ruta + '/' + tipo + '/' + dato);
    }
}